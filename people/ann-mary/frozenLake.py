# frozen lake solver 4*4
# it is a bruteforce approach with the forbidden states explicitly assigned

import gym
import random

env = gym.make("FrozenLake-v0")
env.reset()
env.render()
reward = 0.00

forbidden = [5, 7, 11, 12]

directions = {"left": 0, "down": 1, "right": 2, "up": 3}
c = 0
bool = True
while bool:
    c = c + 1
    win_seq = [
        random.choice(["left", "down", "right"]),
        random.choice(["left", "down", "right"]),
        random.choice(["left", "down"]),
        random.choice(["left", "down", "right", "up"]),
    ]
    for a in win_seq:
        new, reward, done, info = env.step(directions[a])
        print()
        env.render()
        print("Reward: {:.2f}".format(reward))
        if new in forbidden:
            env.reset()
            break
        if new == 15:
            bool = False
            break
print("no.of attempts", c)
print("the winning sequence", win_seq)
